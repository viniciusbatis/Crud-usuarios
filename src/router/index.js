import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login';
import Usuarios from '@/components/Usuarios';
import FormUsuario from '@/components/FormUsuario';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/',
      name: 'Usuarios',
      component: Usuarios,
    },
    {
      path: '/criar-usuario',
      name: 'FormUsuario',
      component: FormUsuario,
    },
    {
      path: '/editar-usuario/:id',
      name: 'EditarUsuario',
      component: FormUsuario,
    },
  ],
});
