import axios from 'axios';

const API_URL = 'https://5b30f4dd7ad3350014b4335a.mockapi.io/usuarios';

export default {
  pegarTodos() {
    return axios.get(API_URL);
  },
  pegarUm(id) {
    return axios.get(`${API_URL}/${id}`);
  },
  criar(usuario) {
    return axios.post(API_URL, usuario);
  },
  editar(id, usuario) {
    return axios.put(`${API_URL}/${id}`, usuario);
  },
  deletar(id) {
    return axios.delete(`${API_URL}/${id}`);
  },
};
